import { ProductsListComponent } from './components/products/products-list/products-list.component';
import { ProductsEffects } from './ngrx/products.effects';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsComponent } from './components/products/products.component';
import { ProductsNavBarComponent } from './components/products/products-nav-bar/products-nav-bar.component';
import { HomeComponent } from './components/home/home.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { productsReducer } from './ngrx/products.reducer';
import { ProductsSearchComponent } from './components/products/products-search/products-search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductsAddComponent } from './components/products/products-add/products-add.component';


@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductsNavBarComponent,
    HomeComponent,
    ProductsSearchComponent,
    ProductsListComponent,
    ProductsAddComponent,
  

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    //on va spécifier ici REDUCER
    //definir le nom de store
    StoreModule.forRoot({ catalogState: productsReducer }),
    //pn va spécifier ici les EFFECTS
    //definir toutes les effcetes de notre application
    EffectsModule.forRoot([ProductsEffects]),
    //Plugin chrome qui permet d'une visiblité ce que ce passe dans le Module NgRX
    StoreDevtoolsModule.instrument(), FormsModule, ReactiveFormsModule],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

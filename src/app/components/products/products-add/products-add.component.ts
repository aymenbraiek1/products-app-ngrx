import { newProductAction, saveProductAction, saveProductSuccessAction } from './../../../ngrx/products.actions';
import { Store } from '@ngrx/store';
import { ProductsState, ProductsStateEnum } from 'src/app/ngrx/products.reducer';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-products-add',
  templateUrl: './products-add.component.html',
  styleUrls: ['./products-add.component.css']
})
export class ProductsAddComponent implements OnInit {
  productformGroup: FormGroup | null = null;
  submitted: boolean = false;
  state: ProductsState | null = null;
  readonly ProductsStateEnum = ProductsStateEnum;
  constructor(private fb: FormBuilder, private productservice: ProductsService, private store: Store<any>) { }

  ngOnInit(): void {
    //alerter le reducer 
    this.store.dispatch(new newProductAction({}));
    this.store.subscribe(state => {
      this.state = state.catalogState;
      if (this.state?.dataState == ProductsStateEnum.NEW) {
        //construire des formulaire
        this.productformGroup = this.fb.group({
          name: ["", Validators.required],
          price: [0, Validators.required],
          quantity: [0, Validators.required],
          selected: [true, Validators.required],
          available: [true, Validators.required]
        });
      }
    })
  }

  OnSaveProduct() {
   // console.log(this.productformGroup?.value);
    this.store.dispatch(new saveProductAction(this.productformGroup?.value))

  }
  newProduct() {
    //lorsque en veut utiliser un action il suffut de faire appel store
    this.store.dispatch(new newProductAction({}));

  }



}

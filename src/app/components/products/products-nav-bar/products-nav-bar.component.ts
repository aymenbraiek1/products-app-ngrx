import { GetAllProductsAction, GetAvailableAllProductsAction, GetSelectedAllProductsAction } from './../../../ngrx/products.actions';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products-nav-bar',
  templateUrl: './products-nav-bar.component.html',
  styleUrls: ['./products-nav-bar.component.css']
})
export class ProductsNavBarComponent implements OnInit {

  constructor(private store: Store<any>,private router:Router) { }

  ngOnInit(): void {

  }
  onGetAllproducts() {
    this.store.dispatch(new GetAllProductsAction({}));

  }
  onGetSelectedproducts() {
    //pas de paramétre
    this.store.dispatch(new GetSelectedAllProductsAction({}));

  }

  onGetAvailableproducts() {

    this.store.dispatch(new GetAvailableAllProductsAction({}));
  }

  onNewproducts() {
    this.router.navigateByUrl("/newProduct");

  }
}

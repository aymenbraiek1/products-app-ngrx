import { deleteProductAction } from './../../../ngrx/products.actions';
import { Store } from '@ngrx/store';
import { Product } from './../../../model/product.model';
import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductsState, ProductsStateEnum } from 'src/app/ngrx/products.reducer';
import { selectProductAction } from 'src/app/ngrx/products.actions';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {
  @Input() productsStatechild$: Observable<ProductsState> | null = null;
  readonly productsDataStateEnum = ProductsStateEnum;
  constructor(private store: Store<any>) { }

  ngOnInit(): void {
  }


  onSelect(product: Product) {
    //console.log(product);
    this.store.dispatch(new selectProductAction(product));
  }

  onDelete(product:Product){
    this.store.dispatch(new deleteProductAction(product));

  }

}

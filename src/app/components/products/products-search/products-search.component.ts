import { searchedProductsActionSuccess, searchProductsAction } from './../../../ngrx/products.actions';
import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products-search',
  templateUrl: './products-search.component.html',
  styleUrls: ['./products-search.component.css']
})
export class ProductsSearchComponent implements OnInit {

  constructor(private store: Store<any>) { }

  ngOnInit(): void {
  }

  onSearch(dataForm: any) {
    this.store.dispatch(new searchProductsAction(dataForm.keyword));
  }

}

import { state } from '@angular/animations';
import { map } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ProductsState, ProductsStateEnum } from 'src/app/ngrx/products.reducer';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  //declare une variable de type Observable 
  //$ pour la convention 
  public productsState$: Observable<ProductsState> | null = null;
  //readonly productsDataStateEnum=ProductsStateEnum;
  constructor(private store: Store<any>) { }

  ngOnInit(): void {
    //toujours sur l'ecout de Store  faire pipe catalogState declarer dans le app.module.ts

    this.productsState$ = this.store.pipe(
      
      map((state) => state.catalogState)
      
    );
    /*this.productsState$=this.store.pipe(
      map((state)=>state.catalogState)
    );
  
  this.productsState$=this.store.pipe(
    map((state)=>state.catalogState)
  )*/
}
}

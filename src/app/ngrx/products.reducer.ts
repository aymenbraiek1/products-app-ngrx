
import { state } from '@angular/animations';
import { Action } from '@ngrx/store';
import { ProductsAction, ProductsActionsTypes } from './products.actions';
import { Product } from './../model/product.model';

//définir enum de toutes les cas 
export enum ProductsStateEnum {
    LOADING = "Loading", LOADED = "Loaded", SUCCESS = "Success", ERROR = "Error", INITIAL = "Initial", NEW = "new", EDIT = "EDIT"
}
// Definir Etat de state initial 
export interface ProductsState {
    products: Product[];
    errorMessage: string;
    dataState: ProductsStateEnum;


}
//initialiser le State le 1 etat que je veux le demarrer 
const initState: ProductsState = {
    products: [],
    errorMessage: " ",
    dataState: ProductsStateEnum.INITIAL
}
//en paramétre je chargé le state à l'etat initial
// valeur de retour de cette function est de type  ProductsState
//Reducer recoit le state actuelle et action produite  qui dispatcher dans le store 
export function productsReducer(state: ProductsState = initState, action: Action): ProductsState {
    //faire le test 
    switch (action.type) {
        //si le cas GET_ALL_PRODUCTS retourner un message que en cours de chargement 
        case ProductsActionsTypes.GET_ALL_PRODUCTS:
            return { ...state, dataState: ProductsStateEnum.LOADING }
        //si le cas est GET_ALL_PRODUCTS_SUCCESS return payload liste des products
        case ProductsActionsTypes.GET_ALL_PRODUCTS_SUCCESS:
            return { ...state, dataState: ProductsStateEnum.LOADED, products: (<ProductsAction>action).payload }
        //si le cas GET_ALL_PRODUCTS_ERROR return le message error
        case ProductsActionsTypes.GET_ALL_PRODUCTS_ERROR:
            return { ...state, dataState: ProductsStateEnum.ERROR, errorMessage: (<ProductsAction>action).payload }
        //si non default return etat state 

        case ProductsActionsTypes.SELECTED_PRODUCTS:
            return { ...state, dataState: ProductsStateEnum.LOADING }
        case ProductsActionsTypes.SELECTED_PRODUCTS_SUCCESS:
            return { ...state, dataState: ProductsStateEnum.LOADED, products: (<ProductsAction>action).payload }
        case ProductsActionsTypes.SELECTED_PRODUCTS_ERROR:
            return { ...state, dataState: ProductsStateEnum.ERROR, errorMessage: (<ProductsAction>action).payload }
        case ProductsActionsTypes.SEARCHED_PRODUCTS:
            return { ...state, dataState: ProductsStateEnum.LOADING }
        case ProductsActionsTypes.SEARCHED_PRODUCTS_SUCCESS:
            return { ...state, dataState: ProductsStateEnum.LOADED, products: (<ProductsAction>action).payload }
        case ProductsActionsTypes.SELECTED_PRODUCTS_ERROR:
            return { ...state, dataState: ProductsStateEnum.ERROR, errorMessage: (<ProductsAction>action).payload }
        case ProductsActionsTypes.AVAILABLE_PRODUCTS:
            return { ...state, dataState: ProductsStateEnum.LOADING }
        case ProductsActionsTypes.AVAILABLE_PRODUCTS_SUCCESS:
            return { ...state, dataState: ProductsStateEnum.LOADED, products: (<ProductsAction>action).payload }
        case ProductsActionsTypes.AVAILABLE_PRODUCTS_ERROR:
            return { ...state, dataState: ProductsStateEnum.ERROR, errorMessage: (<ProductsAction>action).payload }
        case ProductsActionsTypes.SELECT_PRODUCT:
            return { ...state, dataState: ProductsStateEnum.LOADING }
        case ProductsActionsTypes.SELECT_PRODUCT_SUCCESS:
            //le product qui a modifier
            let product: Product = (<ProductsAction>action).payload;
            //recuperer la liste des produits de state
            let listProducts = [...state.products];
            //console.log("p.id" + product.id);
            //data la nouvelle liste
            let data: Product[] = listProducts.map(p => (p.id == product.id) ? product : p);
            //console.log("data" + data);
            return { ...state, dataState: ProductsStateEnum.LOADED, products: data };
        case ProductsActionsTypes.SELECT_PRODUCT_ERROR:
            return { ...state, dataState: ProductsStateEnum.ERROR, errorMessage: (<ProductsAction>action).payload }
        case ProductsActionsTypes.DELETE_PRODUCT:
            return { ...state, dataState: ProductsStateEnum.LOADING }
        case ProductsActionsTypes.DELETE_PRODUCT_SUCCESS:
            let productdel: Product = (<ProductsAction>action).payload;
            let listProductsintial = [...state.products];
            let index = listProductsintial.indexOf(productdel);
             listProductsintial.splice(index,1);
            return {...state, dataState: ProductsStateEnum.LOADED, products: listProductsintial };
        case ProductsActionsTypes.SEARCHED_PRODUCTS_ERROR:
            return { ...state, dataState: ProductsStateEnum.ERROR, errorMessage: (<ProductsAction>action).payload }
        //create new Project
        case ProductsActionsTypes.NEW_PRODUCT:
            return { ...state, dataState: ProductsStateEnum.NEW }
        case ProductsActionsTypes.NEW_PRODUCT_SUCCESS:
            return { ...state, dataState: ProductsStateEnum.NEW }
        case ProductsActionsTypes.NEW_PRODUCT_ERROR:
            return { ...state, dataState: ProductsStateEnum.ERROR, errorMessage: (<ProductsAction>action).payload }
        case ProductsActionsTypes.SAVE_PRODUCT:
            return { ...state, dataState: ProductsStateEnum.LOADING }
        case ProductsActionsTypes.SAVE_PRODUCT_SUCCESS:
            let prods: Product[] = [...state.products];
            prods.push((<ProductsAction>action).payload);
            return { ...state, dataState: ProductsStateEnum.LOADED, products: prods }
        case ProductsActionsTypes.SAVE_PRODUCT_ERROR:
            return { ...state, dataState: ProductsStateEnum.ERROR, errorMessage: (<ProductsAction>action).payload }
        default: return { ...state }

    }


}
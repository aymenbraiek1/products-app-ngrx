import { GetAllProductsActionError, GetAllProductsActionSuccess, GetSelectedAllProductsActionError, GetSelectedAllProductsActionSuccess, ProductsAction, ProductsActionsTypes, GetAvailableProductsSuccessAction, GetAvailableProductsErrorAction, selectProductSuccessAction, selectProductErrorAction, deleteProductSuccessAction, deleteProductErrorAction, newProductSuccessAction, saveProductSuccessAction, saveProductErrorAction } from './products.actions';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { ProductsService } from '../services/products.service';
@Injectable()
export class ProductsEffects {


    constructor(private productsService: ProductsService, private effectsActions: Actions) {

    }
    //la methode est recoit l'action de type Observable 
    //ecoute les actions 
    //recoit dans le corp une function lumda

    getAllProductsEffect: Observable<ProductsAction> = createEffect(
        () => this.effectsActions.pipe(
            //je suis intressé que de ce action
            ofType(ProductsActionsTypes.GET_ALL_PRODUCTS),
            //mapper action avec le service 
            mergeMap((action: ProductsAction) => {
                return this.productsService.getAllProducts()
                    //pipe avec l'operateur map  si succes j'ai créé une action
                    .pipe(
                        //retourner une action 
                        //dans le payload en mettre la liste products
                        map((products) => new GetAllProductsActionSuccess(products)),
                        // si y a un erreur on appel catch error
                        catchError((err) => of(new GetAllProductsActionError(err.message)))
                    )
            })
        )
    );
    getSelectedAllProductsEffect: Observable<ProductsAction> = createEffect(
        () => this.effectsActions.pipe(
            //je suis intressé que de ce action
            ofType(ProductsActionsTypes.SELECTED_PRODUCTS),
            //mapper action avec le service 
            mergeMap((action: ProductsAction) => {
                return this.productsService.getSelectedProducts()
                    //pipe avec l'operateur map  si succes j'ai créé une action
                    .pipe(
                        //retourner une action 
                        //dans le payload en mettre la liste products
                        map((products) => new GetSelectedAllProductsActionSuccess(products)),
                        // si y a un erreur on appel catch error
                        catchError((err) => of(new GetSelectedAllProductsActionError(err.message)))
                    )
            })
        )
    );

    searchedProductsEffect: Observable<ProductsAction> = createEffect(
        () => this.effectsActions.pipe(
            //je suis intressé que de ce action
            ofType(ProductsActionsTypes.SEARCHED_PRODUCTS),
            //mapper action avec le service 
            mergeMap((action: ProductsAction) => {
                return this.productsService.searchProduct(action.payload)
                    //pipe avec l'operateur map  si succes j'ai créé une action
                    .pipe(
                        //retourner une action 
                        //dans le payload en mettre la liste products
                        map((products) => new GetSelectedAllProductsActionSuccess(products)),
                        // si y a un erreur on appel catch error
                        catchError((err) => of(new GetSelectedAllProductsActionError(err.message)))
                    )
            })
        )
    );
    getAvailableAllProductsEffect: Observable<ProductsAction> = createEffect(
        () => this.effectsActions.pipe(
            //je suis intressé que de ce action
            ofType(ProductsActionsTypes.AVAILABLE_PRODUCTS),
            //mapper action avec le service 
            mergeMap((action: ProductsAction) => {
                return this.productsService.getAvailableProducts()
                    //pipe avec l'operateur map  si succes j'ai créé une action
                    .pipe(
                        //retourner une action 
                        //dans le payload en mettre la liste products
                        map((products) => new GetAvailableProductsSuccessAction(products)),
                        // si y a un erreur on appel catch error
                        catchError((err) => of(new GetAvailableProductsErrorAction(err.message)))
                    )
            })
        )
    );
    //select effect implimentation
    selectProductEffect: Observable<ProductsAction> = createEffect(
        () => this.effectsActions.pipe(
            //je suis intressé que de ce action
            ofType(ProductsActionsTypes.SELECT_PRODUCT),
            //mapper action avec le service 
            mergeMap((action: ProductsAction) => {

                return this.productsService.select(action.payload)
                    //pipe avec l'operateur map  si succes j'ai créé une action
                    .pipe(
                        //retourner une action 
                        //dans le payload en mettre la liste products
                        map((product) => new selectProductSuccessAction(product)),
                        // si y a un erreur on appel catch error
                        catchError((err) => of(new selectProductErrorAction(err.message)))
                    )
            })
        )
    );

    //select effect implimentation
    deleteProductEffect: Observable<ProductsAction> = createEffect(
        () => this.effectsActions.pipe(
            //je suis intressé que de ce action
            ofType(ProductsActionsTypes.DELETE_PRODUCT),
            //mapper action avec le service 
            mergeMap((action: ProductsAction) => {
                console.log(action.payload.id);

                return this.productsService.delete(action.payload.id)
                    //pipe avec l'operateur map  si succes j'ai créé une action
                    .pipe(
                        //retourner une action 
                        //dans le payload en mettre la liste products
                        map(() => new deleteProductSuccessAction(action.payload)),
                        // si y a un erreur on appel catch error
                        catchError((err) => of(new deleteProductErrorAction(err.message)))
                    )
            })
        )
    );
    //create new Project 
    newProductEffect: Observable<ProductsAction> = createEffect(
        () => this.effectsActions.pipe(
    
            ofType(ProductsActionsTypes.NEW_PRODUCT),
        map((action:ProductsAction)=>{
            return new newProductSuccessAction({})
        })));
   //select effect implimentation
   saveProductEffect: Observable<ProductsAction> = createEffect(
    () => this.effectsActions.pipe(
        //je suis intressé que de ce action
        ofType(ProductsActionsTypes.SAVE_PRODUCT),
        //mapper action avec le service 
        mergeMap((action: ProductsAction) => {

            return this.productsService.save(action.payload)
                //pipe avec l'operateur map  si succes j'ai créé une action
                .pipe(
                    //retourner une action 
                    //dans le payload en mettre la liste products
                    map(() => new saveProductSuccessAction(action.payload)),
                    // si y a un erreur on appel catch error
                    catchError((err) => of(new saveProductErrorAction(err.message)))
                )
        })
    )
);

    }

import { Product } from './../model/product.model';
import { Action } from "@ngrx/store";


export enum ProductsActionsTypes {
    //par convention affecter un string  pour chaque action on a trois cas 
    GET_ALL_PRODUCTS = "[Products] Get all products",
    GET_ALL_PRODUCTS_SUCCESS = "[Products] Get all products Success",
    GET_ALL_PRODUCTS_ERROR = "[Products] Get all products Error",
    // Get selected Products
    SELECTED_PRODUCTS = "[Products]  Get selected  products",
    SELECTED_PRODUCTS_SUCCESS = "[Products] Get selected products Success",
    SELECTED_PRODUCTS_ERROR = "[Products] Get selected  products Error",
    //serach with keyword
    SEARCHED_PRODUCTS = "[Products]  Get searched  products",
    SEARCHED_PRODUCTS_SUCCESS = "[Products] Get searched products Success",
    SEARCHED_PRODUCTS_ERROR = "[Products] Get searched  products Error",
    //AVAILABLE PRODUCTS
    AVAILABLE_PRODUCTS = "[Products]  Get available  products",
    AVAILABLE_PRODUCTS_SUCCESS = "[Products]  Get available  products success",
    AVAILABLE_PRODUCTS_ERROR = "[Products]  Get available  products error",
    //select product
    SELECT_PRODUCT = "[Products]   select  product",
    SELECT_PRODUCT_SUCCESS = "[Products] select product Success",
    SELECT_PRODUCT_ERROR = "[Products] select  product Error",
    //Delete Product
    DELETE_PRODUCT = "[Products]   delete  product",
    DELETE_PRODUCT_SUCCESS = "[Products] delete product Success",
    DELETE_PRODUCT_ERROR = "[Products] delete  product Error",
    //New Product
    NEW_PRODUCT = "[Products] create new Product",
    NEW_PRODUCT_SUCCESS = "[Products] create new Product success",
    NEW_PRODUCT_ERROR = "[Products] new  product Error",
    //save Product
    SAVE_PRODUCT = "[Products] Save new Product",
    SAVE_PRODUCT_SUCCESS = "[Products] Save  Product success",
    SAVE_PRODUCT_ERROR = "[Products] Save  product Error",
}

export class GetAllProductsAction implements Action {

    //definit par 2 attributs type et payload  
    type: ProductsActionsTypes = ProductsActionsTypes.GET_ALL_PRODUCTS;
    //bon pratique de définit payload dans le constructeur
    //pour Action Get All pas des paramtres dans notre requette donc on définit any 
    constructor(public payload: any) {

    }
}
export class GetAllProductsActionError implements Action {

    //definit par 2 attributs type et payload  
    type: ProductsActionsTypes = ProductsActionsTypes.GET_ALL_PRODUCTS_ERROR;
    //bon pratique de définit payload dans le constructeur
    //pour playload est de type de string message error 
    constructor(public payload: string) {

    }
}
export class GetAllProductsActionSuccess implements Action {

    //definit par 2 attributs type et payload  
    type: ProductsActionsTypes = ProductsActionsTypes.GET_ALL_PRODUCTS_SUCCESS;
    //bon pratique de définit payload dans le constructeur
    //pour playload est de type de string message success 
    //succes donc le payload est une liste de products
    constructor(public payload: Product[]) {

    }
}

export class GetSelectedAllProductsAction implements Action {

    //definit par 2 attributs type et payload  
    type: ProductsActionsTypes = ProductsActionsTypes.SELECTED_PRODUCTS;
    //bon pratique de définit payload dans le constructeur
    //pour Action Get All pas des paramtres dans notre requette donc on définit any 
    constructor(public payload: any) {

    }
}
export class GetSelectedAllProductsActionSuccess implements Action {

    //definit par 2 attributs type et payload  
    type: ProductsActionsTypes = ProductsActionsTypes.SELECTED_PRODUCTS_SUCCESS;
    //bon pratique de définit payload dans le constructeur
    //pour playload est de type de string message success 
    //succes donc le payload est une liste de products
    constructor(public payload: Product[]) {

    }
}

export class GetSelectedAllProductsActionError implements Action {

    //definit par 2 attributs type et payload  
    type: ProductsActionsTypes = ProductsActionsTypes.SELECTED_PRODUCTS_ERROR;
    //bon pratique de définit payload dans le constructeur
    //pour playload est de type de string message error 
    constructor(public payload: string) {

    }
}
export class searchProductsAction implements Action {

    //definit par 2 attributs type et payload  
    type: ProductsActionsTypes = ProductsActionsTypes.SEARCHED_PRODUCTS;
    //bon pratique de définit payload dans le constructeur
    //pour Action Get All pas des paramtres dans notre requette donc on définit any 
    constructor(public payload: any) {

    }
}
export class searchedProductsActionSuccess implements Action {

    //definit par 2 attributs type et payload  
    type: ProductsActionsTypes = ProductsActionsTypes.SEARCHED_PRODUCTS_SUCCESS;
    //bon pratique de définit payload dans le constructeur
    //pour playload est de type de string message success 
    //succes donc le payload est une liste de products
    constructor(public payload: Product[]) {

    }
}

export class searchedProductsActionError implements Action {

    //definit par 2 attributs type et payload  
    type: ProductsActionsTypes = ProductsActionsTypes.SEARCHED_PRODUCTS_ERROR;
    //bon pratique de définit payload dans le constructeur
    //pour playload est de type de string message error 
    constructor(public payload: string) {

    }
}

export class GetAvailableAllProductsAction implements Action {

    //definit par 2 attributs type et payload  
    type: ProductsActionsTypes = ProductsActionsTypes.AVAILABLE_PRODUCTS;
    //bon pratique de définit payload dans le constructeur
    //pour Action Get All pas des paramtres dans notre requette donc on définit any 
    constructor(public payload: any) {

    }
}

export class GetAvailableProductsSuccessAction implements Action {

    //definit par 2 attributs type et payload  
    type: ProductsActionsTypes = ProductsActionsTypes.AVAILABLE_PRODUCTS_SUCCESS;
    //bon pratique de définit payload dans le constructeur
    //pour Action Get All pas des paramtres dans notre requette donc on définit any 
    constructor(public payload: Product[]) {

    }
}
export class GetAvailableProductsErrorAction implements Action {

    //definit par 2 attributs type et payload  
    type: ProductsActionsTypes = ProductsActionsTypes.AVAILABLE_PRODUCTS_ERROR;
    //bon pratique de définit payload dans le constructeur
    //pour playload est de type de string message error 
    constructor(public payload: string) {

    }
}

export class selectProductAction implements Action {
    type: ProductsActionsTypes = ProductsActionsTypes.SELECT_PRODUCT;
    constructor(public payload: Product) {

    }
}
export class selectProductSuccessAction implements Action {
    type: ProductsActionsTypes = ProductsActionsTypes.SELECT_PRODUCT_SUCCESS;
    constructor(public payload: Product) {

    }
}

export class selectProductErrorAction implements Action {
    type: ProductsActionsTypes = ProductsActionsTypes.SELECT_PRODUCT_ERROR;
    constructor(public payload: string) {

    }
}
export class deleteProductAction implements Action {
    type: ProductsActionsTypes = ProductsActionsTypes.DELETE_PRODUCT;
    constructor(public payload: Product) {

    }
}
export class deleteProductSuccessAction implements Action {
    type: ProductsActionsTypes = ProductsActionsTypes.DELETE_PRODUCT_SUCCESS;
    constructor(public payload: Product) {

    }
}
export class deleteProductErrorAction implements Action {
    type: ProductsActionsTypes = ProductsActionsTypes.DELETE_PRODUCT_ERROR;
    constructor(public payload: string) {

    }
}
export class newProductAction implements Action {
    type: ProductsActionsTypes = ProductsActionsTypes.NEW_PRODUCT;
    constructor(public payload: any) {

    }
}
export class newProductSuccessAction implements Action {
    type: ProductsActionsTypes = ProductsActionsTypes.NEW_PRODUCT_SUCCESS;
    constructor(public payload: any) {

    }
}
export class newProductErrorAction implements Action {
    type: ProductsActionsTypes = ProductsActionsTypes.NEW_PRODUCT_ERROR;
    constructor(public payload: string) {

    }
}
export class saveProductAction implements Action {
    type: ProductsActionsTypes = ProductsActionsTypes.SAVE_PRODUCT;
    constructor(public payload: Product) {

    }
}
export class saveProductSuccessAction implements Action {
    type: ProductsActionsTypes = ProductsActionsTypes.SAVE_PRODUCT_SUCCESS;
    constructor(public payload: Product) {

    }
}
export class saveProductErrorAction implements Action {
    type: ProductsActionsTypes = ProductsActionsTypes.SAVE_PRODUCT_ERROR;
    constructor(public payload: string) {

    }
}
/*   traitement de l'action selected  */
//définir un type de action 
export type ProductsAction = GetAllProductsAction | GetAllProductsActionError
    | GetAllProductsActionSuccess | GetSelectedAllProductsAction
    | GetSelectedAllProductsActionError | GetSelectedAllProductsActionSuccess
    | searchProductsAction | searchedProductsActionError | searchedProductsActionSuccess | GetAvailableAllProductsAction
    | GetAvailableProductsSuccessAction
    | GetAvailableProductsErrorAction
    | selectProductAction | selectProductErrorAction | selectProductAction
    | deleteProductAction | deleteProductSuccessAction | deleteProductErrorAction
    | newProductAction | newProductErrorAction | newProductSuccessAction
    |saveProductAction|saveProductErrorAction|saveProductSuccessAction;

import { Product } from './../model/product.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

//Injectable this service is enabled to root racine
//pour visualiser seulement dans un component on va le declarer dans provider array
@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  // Dependency Injection http
  constructor(private http: HttpClient) { }


  //getall products to service
  //on sait que notre methodes et retouner un objet de type observable contient liste de produits
  getAllProducts(): Observable<Product[]> {
    let host = environment.host;
    //methode get declarer le type => j attend une liste des produits 
    return this.http.get<Product[]>(host + "/products")
  }

  //method qui definit que les produits selected 
  getSelectedProducts(): Observable<Product[]> {
    let host = environment.host;
    return this.http.get<Product[]>(host + "/products?selected=true");
  }

  //method qui definit que les produits selected 
  getAvailableProducts(): Observable<Product[]> {
    let host = environment.host;
    return this.http.get<Product[]>(host + "/products?available=true");
  }

  searchProduct(keyword: string): Observable<Product[]> {
    let host = environment.host;

    return this.http.get<Product[]>(host + "/products?name_like=" + keyword);
  }
  select(product: Product): Observable<Product> {

    return this.http.put<Product>(environment.host + "/products/" + product.id, { ...product, selected: !product.selected });
  }

  public delete(id:number):Observable<void>{
    return this.http.delete<void>(environment.host+"/products/"+id);
 }
  save(product: Product): Observable<Product> {

    return this.http.post<Product>(environment.host + "/products/",product);
  }

  Edit(product: Product): Observable<Product> {
    let host = environment.host;
    product.selected = !product.selected;
    return this.http.put<Product>(host + "/products/" + product.id, product);
    //payload paramétre de notre requette
  }

  getProducts(id: number): Observable<Product> {
    let host = environment.host;
    return this.http.get<Product>(host + "/products/" + id);
  }

  UpdateProducts(p: Product): Observable<Product> {
    let host = environment.host;
    return this.http.put<Product>(host + "/products/" + p.id, p);
  }
}




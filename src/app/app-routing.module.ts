import { ProductsAddComponent } from './components/products/products-add/products-add.component';
import { HomeComponent } from './components/home/home.component';
import { ProductsComponent } from './components/products/products.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: "products", component: ProductsComponent },
  { path: " ", component: HomeComponent },
  {path:"newProduct",component:ProductsAddComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
